# Especificación con ejemplos usando Cucumber

Modelado de pago de pasaje de colectivo utilizando una tarjeta SUBE.

Casos de uso:

* Como un pasajero con suficiente saldo en mi tarjeta SUBE, quiero pagar el pasaje de colectivo para poder viajar
* Como un pasajero con suficiente saldo en descubierto en mi tarjeta SUBE, quiero pagar el pasaje de colectivo para poder viajar
