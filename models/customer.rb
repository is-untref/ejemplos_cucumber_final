class Customer
  attr_reader :sube_card

  def initialize(sube_card)
    @sube_card = sube_card
  end

  def pay(amount)
    @sube_card.pay(amount)
  end
end
