class SubeCard
  attr_reader :balance
  attr_accessor :maximum_overdraft

  def initialize(balance)
    @balance = balance
    @maximum_overdraft = 0
  end

  def pay(amount)
    if (@balance + @maximum_overdraft < amount)
      false
    else
      @balance -= amount
      true
    end
  end
end
