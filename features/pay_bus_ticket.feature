# language: es
Característica: Paga el boleto del colectivo

Ejemplo: La pasajera paga con crédito en su tarjeta SUBE
  Dado una pasajera que tiene un saldo de $300 en su tarjeta SUBE
  Cuando ella paga el boleto de $100 con su tarjeta SUBE
  Entonces se emite el boleto
  Y el saldo en la tarjeta SUBE es $200

Ejemplo: La pasajera paga con el saldo en descubierto de su tarjeta SUBE
  Dado una pasajera que tiene un saldo de $50 en su tarjeta SUBE
  Y el saldo en descubierto máximo es $100
  Cuando ella paga el boleto de $100 con su tarjeta SUBE
  Entonces se emite el boleto
  Y el saldo en la tarjeta SUBE es $-50

Ejemplo: La pasajera no tiene suficiente crédito en su tarjeta SUBE
  Dado una pasajera que tiene un saldo de $-80 en su tarjeta SUBE
  Y el saldo en descubierto máximo es $100
  Cuando ella paga el boleto de $100 con su tarjeta SUBE
  Entonces no se emite el boleto
  Y el saldo en la tarjeta SUBE es $-80
