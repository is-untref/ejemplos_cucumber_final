require_relative '../support/env'

Given("una pasajera que tiene un saldo de ${int} en su tarjeta SUBE") do |balance|
  @sube_card = SubeCard.new(balance.to_i)
  @customer = Customer.new(@sube_card)
end

Given("el saldo en descubierto máximo es ${int}") do |maximum_overdraft|
  @sube_card.maximum_overdraft = maximum_overdraft.to_i
end

When("ella paga el boleto de ${int} con su tarjeta SUBE") do |ticket_cost|
  @payment_result = @customer.pay(ticket_cost.to_i)
end

Then("se emite el boleto") do
  expect(@payment_result).to be_truthy
end

Then("el saldo en la tarjeta SUBE es ${int}") do |expected_sube_card_balance|
  expect(@sube_card.balance).to eq(expected_sube_card_balance.to_i)
end

Then("no se emite el boleto") do
  expect(@payment_result).to be_falsey
end
